
function check(){

	var question1 = document.quiz.question1.value;
	var question2 = document.quiz.question2.value;
	var question3 = document.quiz.question3.value;
	var question4 = document.quiz.question4.value;
	var question5 = document.quiz.question5.value;
	var question6 = document.quiz.question6.value;
	var question7 = document.quiz.question7.value;
	var question8 = document.quiz.question8.value;
	var question9 = document.quiz.question9.value;
	var question10 = document.quiz.question10.value;
	var correct = 0;


	if (question1 == "Great Wall of China") {
		correct++;
    }
    else{
	document.getElementById("one").style.color = "red";
	document.getElementById("one").style.display = "block";	
	}

	if (question2 == "The Pyramids of Egypt") {
		correct++;
    }
    else{
	document.getElementById("two").style.color = "red";
	document.getElementById("two").style.display = "block";	
	}

	if (question3 == "Sostratus of Cnidus") {
		correct++;
    }
    else{
	document.getElementById("three").style.color = "red";
	document.getElementById("three").style.display = "block";	
	}

	if (question4 == "The Eiffel Tower in Paris") {
		correct++;
    }
    else{
	document.getElementById("four").style.color = "red";
	document.getElementById("four").style.display = "block";	
	}

	if (question5 == "Pachacutec") {
		correct++;
    }
    else{
	document.getElementById("five").style.color = "red";
	document.getElementById("five").style.display = "block";	
	}

	if (question6 == "Heitor de Silva Costa") {
		correct++;
    }
    else{
	document.getElementById("six").style.color = "red";
	document.getElementById("six").style.display = "block";	
	}

	if (question7 == "Rome") {
		correct++;
    }
    else{
	document.getElementById("seven").style.color = "red";
	document.getElementById("seven").style.display = "block";	
	}

	if (question8 == "Petra") {
		correct++;
    }	else{
	document.getElementById("eight").style.color = "red";
	document.getElementById("eight").style.display = "block";	
	}
	if (question9 == "The Hanging Gardens of Babylon") {
		correct++;
    }	else{
	document.getElementById("nine").style.color = "red";
	document.getElementById("nine").style.display = "block";	
	}
	if (question10 == "Wife") {
		correct++;
    }
	else{
	document.getElementById("ten").style.color = "red";
	document.getElementById("ten").style.display = "block";	
	}
	
	var pictures = ["img/win.gif", "img/meh.jpeg", "img/lose.gif"];
    var messages = ["Great job!", "That's just okay", "You really need to do better"];
	var score;

	if (correct == 0) {
		score = 2;
	}

	if (correct > 0 && correct < 8) {
		score = 1;
	}

	if (correct > 7 && correct < 11) {
		score = 0;
	}

	document.getElementById("after_submit").style.visibility = "visible";
	document.getElementById("mc").style.color = "red";
	document.getElementById("message").innerHTML = messages[score];
	document.getElementById("number_correct").innerHTML = "You got " + correct + " correct.";
	document.getElementById("picture").src = pictures[score];
	}
	
